### Execução do teste - Luan Messias
Para executar o teste trabalhei com as ferramentas e tecnologias:

VSCODE como editor.
SASS como pré-processador.
Grunt como task-runner.
AngularJs para scripts.

• Os arquivos SASS e Grunt estão dentro do diretorio Deploy na raiz do projeto.

Mesmo o projeto sendo um pouco pequeno decidi modularizar o SASS e separar seus componentes em arquivos e componentes internos,
todo arquivo global pussui um grupo de placeholders separados e abaixo suas respectivas chamadas, deste modo consigo nomear todos
os componentes de forma mais amigavel e tornar os compontes mais independentes.

Teste em produção: http://rankingfazenda.luanmessias.com/

